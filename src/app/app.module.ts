import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CursosListaComponent } from "./cursos-lista/cursos-lista.component";
import { HttpClientModule } from '@angular/common/http';
import { CursosFormComponent } from './cursos-form/cursos-form.component';
import { CursoService } from './service/curso.service';
import { ModalModule } from 'ngx-bootstrap/modal';


@NgModule({
  declarations: [
    AppComponent,
    CursosListaComponent,
    CursosFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  providers: [CursoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
