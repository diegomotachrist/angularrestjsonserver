import { Component, OnInit, ViewChild} from '@angular/core';
import { CursoService } from '../service/curso.service';
import { Curso } from './curso';
import { Observable, empty } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal'; 
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-cursos-lista',
  templateUrl: './cursos-lista.component.html',
  styleUrls: ['./cursos-lista.component.css'],
  preserveWhitespaces: true
})

export class CursosListaComponent implements OnInit { 

  //cursos: Curso[];
  cursos$: Observable<Curso[]>;
  deleteModalRef: BsModalRef;
  @ViewChild('deleteModal') deleteModal;
  cursoDeleteSelected: Curso;

  constructor( private service: CursoService,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: BsModalService) { }

  ngOnInit() {
    //this.service.list().subscribe(dados => this.cursos = dados);
  
    this.cursos$ = this.service.list();
  }

  onRefresh() {
    this.cursos$ = this.service.list().pipe(
      // map(),
      // tap(),
      // switchMap(),
      catchError(error => {
        console.error(error);
        // this.error$.next(true);
        this.handleError();
        return empty();
      })
    );
  }
  handleError() {
    throw new Error("Method not implemented.");
  }

  onEdit(id){
    this.router.navigate(['editar',id], {relativeTo: this.route});
  }

  onDelete(curso){
    this.cursoDeleteSelected = curso;
    this.deleteModalRef = this.modalService.show(this.deleteModal, {class:'modal-sm'});
  }

  onConfirmDelete(){
    this.service.remove(this.cursoDeleteSelected.id).subscribe(
      success => {
        this.deleteModalRef.hide();
        this.onRefresh();
      }, 
      error => console.log(error)
    );
  }

  onDeclineDelete() {
    this.deleteModalRef.hide();
  }

}
