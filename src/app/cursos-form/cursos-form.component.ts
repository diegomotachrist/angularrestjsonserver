import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CursoService } from '../service/curso.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cursos-form',
  templateUrl: './cursos-form.component.html',
  styleUrls: ['./cursos-form.component.css']
})
export class CursosFormComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder,
    private service: CursoService,
    private location: Location,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.params.subscribe(
      (params: any) => {
        const id = params['id'];
        console.log(id);
        const curso$ = this.service.loadByID(id);
        curso$.subscribe(curso => {
          this.updateForm(curso);
        });
      }
    )

    this.form = this.fb.group({
      id: [null],
      nome: [null, Validators.required]
    });
  }

  updateForm(curso) {
    this.form.patchValue({
      id: curso.id,
      nome: curso.nome
    });
  }

  hasError(field: string) {
    return this.form.get(field).errors;
  }

  onSubmit() {
    this.submitted = true;
    if (this.form.valid) {
      if (this.form.value.id) {        
        this.service.update(this.form.value).subscribe(
          success => {
            console.log('sucesso');
            this.location.back();
          },
          error => console.error(error),
          () => console.log('update ok')
        )
      } else {
        this.service.create(this.form.value).subscribe(
          success => {
            console.log('sucesso');
            this.location.back();
          },
          error => console.error(error),
          () => console.log('request ok')
        );
      }
    }
  }

  onCancel() {
    this.submitted = false;
    this.form.reset();
  }
}
